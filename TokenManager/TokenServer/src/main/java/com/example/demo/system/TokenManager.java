package com.example.demo.system;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * A class for managing token information like ownership and validity.
 *
 */
public class TokenManager {
    
    private TokenRepository repository;
    
    public TokenManager(TokenRepository repository) {
        this.repository = repository;
    }
	
    /**
     * Generates and stores a list of tokens that can be used for
     * payments issued by the customer
     * 
     * @param cpr customer issuing the request
     * @param numberOfTokens the number of requested tokens
     * @return
     */
    public List<Token> requestTokens(String cpr, int numberOfTokens) {
        List<Token> result = new ArrayList<>();
        
        // TODO: validate CPR
        if (numberOfTokens >= 0) {
            for (int i = 0; i < numberOfTokens; i++) {
                Token token = new Token();
                result.add(token);
                repository.storeToken(token, cpr);
            }
        }
	    
        return result;
    }
    
    /**
     * Indicates whether the token belongs to the customer.
     * Provides no information whether the token has been used.
     * 
     * @param cpr customer ID in question
     * @param token token in question
     * @return true if the token belongs to this customer,
     *          false otherwise
     */
    private boolean isOwnershipValid(String cpr, Token token) {
        return repository.isOwnershipValid(token, cpr);
    }
    
    /**
     * Indicates that the token with this ID has already been used for a payment.
     * 
     * @param token object representation of the used token
     */
    public void markTokenUsed(Token token) {
        repository.markTokenUsed(token);
    }
    
    /**
     * Indicates that the token with this ID has already been used for a payment.
     * 
     * @param tokenId identifier of the used token
     */
    public void markTokenUsed(String tokenId) {
        Token token = new Token(tokenId);
        markTokenUsed(token);
    }
    
    /**
     * Whether the token with this ID has already been used for a payment.
     * 
     * @param token object representation of the token
     */
    public boolean isTokenUsed(Token token) {
        return repository.isTokenUsed(token);
    }
    
    /**
     * Whether the token with this ID has already been used for a payment.
     * 
     * @param tokenId identifier of the token
     */
    public boolean getTokenStatus(String tokenId) {
        Token token = new Token(tokenId);
        return isTokenUsed(token);
    }

}
	
	
