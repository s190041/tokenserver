package com.example.demo.system;

/**
 * Class representation of a request for issuing a token (server side).
 * @author Hubert Baumeister
 *
 */
public class TokenRequest {
	
    /**
     * Customer issuing this token request.
     */
	private String CPR;
	
	/**
	 * Number of requested tokens.
	 */
	private int number;
	
	public String getCpr() {
		return CPR;
	}
	public void setCpr(String cpr) {
		this.CPR = cpr;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	} 

}