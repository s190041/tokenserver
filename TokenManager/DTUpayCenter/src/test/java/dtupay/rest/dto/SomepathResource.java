package dtupay.rest.dto;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import helloservice.TokenServer;

/* Note that the resource names and the exclusive use of HTTP POST is on purpose.
 * You should be designing the right resource URI's and use the correct HTTP verb yourself.
 */
@Path("/dtupay")
public class SomepathResource {
	
	//private static TokenServer dtuPay = new TokenServer(new BankServiceService().getBankServicePort());
	
	
	@GET
	@Path("test")
	@Produces("text/plain")
	public String test() {
		
		return "yo yo yo";
	}
}