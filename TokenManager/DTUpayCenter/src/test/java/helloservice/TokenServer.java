package helloservice;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import dtupay.rest.dto.Token;
import dtupay.rest.dto.TokenRequest;


public class TokenServer {

	WebTarget baseUrl;
	private String tokenServicePath = "TokenService";
	
	public TokenServer() {
		Client client = ClientBuilder.newClient();
        this.baseUrl = client.target("http://localhost:8080/");
	}
	
	
	public void markTokenUsed(Token token) {
		baseUrl.path(tokenServicePath)
    		.path(String.format("tokens/%s", token.getTokenId()))
    		.request()
    		.post(Entity.entity(token.getTokenId(), "application/json"));
	}
	
	public List<Token> requestTokens(String cprNumber, int i) {
		TokenRequest tr = new TokenRequest();
		tr.setCpr(cprNumber);
		tr.setNumber(i);
		
		Token[] tokens = baseUrl.path(tokenServicePath)
    		.path("tokens")
    		.request()
    		.accept(MediaType.APPLICATION_JSON)
    		.post(Entity.entity(tr, "application/json"), Token[].class);
		
		return Arrays.asList(tokens);
	}
	
	public boolean getTokenStatus(Token token) {
        boolean result =
        baseUrl.path(tokenServicePath)
    		.path(String.format("tokens/%s", token.getTokenId()))
    		.request()
    		.accept(MediaType.TEXT_PLAIN)
    		.get(Boolean.class);
        
        return result; 
	}
	
}
	