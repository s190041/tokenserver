package helloservice;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import dtupay.rest.dto.Token;
import helloservice.TokenServer;

import java.util.List;

public class TokenServiceSteps {

	private TokenServer tokenServer = new TokenServer();
	private Token realToken;
	private String cprNumber;
	private List<Token> tokens;
	private int i;
	private boolean status;
	
	@Given("that the client requests one token")
	public void requestATestToken() {
		cprNumber = "280780-0728";
		i = 1;
	    tokens = tokenServer.requestTokens(cprNumber, i);
	}

	@Given("the client marks the token as used")
	public void markTokenUsed() {
	    realToken = tokens.get(0);
	    tokenServer.markTokenUsed(realToken);
	}

	@When("the token status is requested")
	public void getTokenStatus() {
	    status = tokenServer.getTokenStatus(realToken);
	}

	@Then("the token status is marked as used")
	public void statusOfTokenIsUsed() {
	    Assert.assertTrue(status);
	}
	
}
