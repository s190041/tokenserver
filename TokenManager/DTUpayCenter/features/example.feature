#Author: your.email@your.domain.com

@tokenPayment
Feature: Token service

  @testTokenService
  Scenario: Successful token generation
    Given that the client requests one token
    And the client marks the token as used
    When the token status is requested
    Then the token status is marked as used
