#!/bin/bash
# Author: Hubert Baumeister
set -e

pushd TokenServer

mvn clean package

# Build the docker image using
# dtupayserver/Dockerfile
docker build --tag tokenservice .

# Do a garbage collection of images not used anymore.
docker image prune -f 

popd

pushd DTUpayCenter

# Start all the services defined in the
# demo-client/docker-compose.yaml file.
# The file refers to image that are locally or globally available.
# The image demo exist, because we have just build it.
# If the demo container is already running, and the image has changed,
# then docker-compose will create a new container from that image and
# replace the old container with the new container.
# Start docker-compose up with the -d option to run
# docker-compose in the background.
docker-compose up -d

# Give Thorntail a chance to be ready.
# Increase sleep time as startup can be slow
sleep 120s

mvn clean test

popd
